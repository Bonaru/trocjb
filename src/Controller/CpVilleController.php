<?php

namespace App\Controller;

use App\Entity\CpVille;
use App\Form\CpVilleType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cp/ville")
 */
class CpVilleController extends AbstractController
{
    /**
     * @Route("/", name="cp_ville_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $cpVilles = $entityManager
            ->getRepository(CpVille::class)
            ->findAll();

        return $this->render('cp_ville/index.html.twig', [
            'cp_villes' => $cpVilles,
        ]);
    }

    /**
     * @Route("/new", name="cp_ville_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $cpVille = new CpVille();
        $form = $this->createForm(CpVilleType::class, $cpVille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($cpVille);
            $entityManager->flush();

            return $this->redirectToRoute('cp_ville_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cp_ville/new.html.twig', [
            'cp_ville' => $cpVille,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idville}", name="cp_ville_show", methods={"GET"})
     */
    public function show(CpVille $cpVille): Response
    {
        return $this->render('cp_ville/show.html.twig', [
            'cp_ville' => $cpVille,
        ]);
    }

    /**
     * @Route("/{idville}/edit", name="cp_ville_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CpVille $cpVille, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CpVilleType::class, $cpVille);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('cp_ville_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('cp_ville/edit.html.twig', [
            'cp_ville' => $cpVille,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idville}", name="cp_ville_delete", methods={"POST"})
     */
    public function delete(Request $request, CpVille $cpVille, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$cpVille->getIdville(), $request->request->get('_token'))) {
            $entityManager->remove($cpVille);
            $entityManager->flush();
        }

        return $this->redirectToRoute('cp_ville_index', [], Response::HTTP_SEE_OTHER);
    }
}
