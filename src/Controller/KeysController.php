<?php

namespace App\Controller;

use App\Entity\Keys;
use App\Form\KeysType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t_keys")
 */
class KeysController extends AbstractController
{
    /**
     * @Route("/", name="keys_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $keys = $entityManager
            ->getRepository(Keys::class)
            ->findAll();

        return $this->render('keys/index.html.twig', [
            'keys' => $keys,
        ]);
    }

    /**
     * @Route("/new", name="keys_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $key = new Keys();
        $form = $this->createForm(KeysType::class, $key);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($key);
            $entityManager->flush();

            return $this->redirectToRoute('keys_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('keys/new.html.twig', [
            'key' => $key,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idkeys}", name="keys_show", methods={"GET"})
     */
    public function show(Keys $key): Response
    {
        return $this->render('keys/show.html.twig', [
            'key' => $key,
        ]);
    }

    /**
     * @Route("/{idkeys}/edit", name="keys_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Keys $key, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(KeysType::class, $key);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('keys_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('keys/edit.html.twig', [
            'key' => $key,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idkeys}", name="keys_delete", methods={"POST"})
     */
    public function delete(Request $request, Keys $key, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $key->getIdkeys(), $request->request->get('_token'))) {
            $entityManager->remove($key);
            $entityManager->flush();
        }

        return $this->redirectToRoute('keys_index', [], Response::HTTP_SEE_OTHER);
    }
}