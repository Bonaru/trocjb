<?php

namespace App\Controller;

use App\Entity\CaractObjets;
use App\Form\CaractObjetsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/caract/objets")
 */
class CaractObjetsController extends AbstractController
{
    /**
     * @Route("/", name="caract_objets_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $caractObjets = $entityManager
            ->getRepository(CaractObjets::class)
            ->findAll();

        return $this->render('caract_objets/index.html.twig', [
            'caract_objets' => $caractObjets,
        ]);
    }

    /**
     * @Route("/new", name="caract_objets_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $caractObjet = new CaractObjets();
        $form = $this->createForm(CaractObjetsType::class, $caractObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($caractObjet);
            $entityManager->flush();

            return $this->redirectToRoute('caract_objets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('caract_objets/new.html.twig', [
            'caract_objet' => $caractObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idobject}", name="caract_objets_show", methods={"GET"})
     */
    public function show(CaractObjets $caractObjet): Response
    {
        return $this->render('caract_objets/show.html.twig', [
            'caract_objet' => $caractObjet,
        ]);
    }

    /**
     * @Route("/{idobject}/edit", name="caract_objets_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CaractObjets $caractObjet, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CaractObjetsType::class, $caractObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('caract_objets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('caract_objets/edit.html.twig', [
            'caract_objet' => $caractObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{idobject}", name="caract_objets_delete", methods={"POST"})
     */
    public function delete(Request $request, CaractObjets $caractObjet, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$caractObjet->getIdobject(), $request->request->get('_token'))) {
            $entityManager->remove($caractObjet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('caract_objets_index', [], Response::HTTP_SEE_OTHER);
    }
}
