<?php

namespace App\Controller;

use App\Entity\CompteUser;
use App\Form\CompteUserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/compte/user")
 */
class CompteUserController extends AbstractController
{
    /**
     * @Route("/", name="compte_user_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $compteUsers = $entityManager
            ->getRepository(CompteUser::class)
            ->findAll();

        return $this->render('compte_user/index.html.twig', [
            'compte_users' => $compteUsers,
        ]);
    }

    /**
     * @Route("/new", name="compte_user_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $compteUser = new CompteUser();
        $form = $this->createForm(CompteUserType::class, $compteUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($compteUser);
            $entityManager->flush();

            return $this->redirectToRoute('compte_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('compte_user/new.html.twig', [
            'compte_user' => $compteUser,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{iduser}", name="compte_user_show", methods={"GET"})
     */
    public function show(CompteUser $compteUser): Response
    {
        return $this->render('compte_user/show.html.twig', [
            'compte_user' => $compteUser,
        ]);
    }

    /**
     * @Route("/{iduser}/edit", name="compte_user_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CompteUser $compteUser, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CompteUserType::class, $compteUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('compte_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('compte_user/edit.html.twig', [
            'compte_user' => $compteUser,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{iduser}", name="compte_user_delete", methods={"POST"})
     */
    public function delete(Request $request, CompteUser $compteUser, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$compteUser->getIduser(), $request->request->get('_token'))) {
            $entityManager->remove($compteUser);
            $entityManager->flush();
        }

        return $this->redirectToRoute('compte_user_index', [], Response::HTTP_SEE_OTHER);
    }
}
