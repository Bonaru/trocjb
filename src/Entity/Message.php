<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Message
 *
 * @ORM\Table(name="message", indexes={@ORM\Index(name="recepteur_idx", columns={"recepteur"}), @ORM\Index(name="emetteur_idx", columns={"emetteur"}), @ORM\Index(name="STATUT_idx", columns={"statut"})})
 * @ORM\Entity
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmessage", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idmessage;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="string", length=45, nullable=false)
     */
    private $objet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $date = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="piece_jointe", type="string", length=50, nullable=false)
     */
    private $pieceJointe;

    /**
     * @var \Statut
     *
     * @ORM\ManyToOne(targetEntity="Statut")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statut", referencedColumnName="idstatut")
     * })
     */
    private $statut;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recepteur", referencedColumnName="user_id")
     * })
     */
    private $recepteur;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="emetteur", referencedColumnName="user_id")
     * })
     */
    private $emetteur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Objets", mappedBy="idmessage")
     */
    private $idobjet;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idobjet = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdmessage(): ?int
    {
        return $this->idmessage;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getPieceJointe(): ?string
    {
        return $this->pieceJointe;
    }

    public function setPieceJointe(string $pieceJointe): self
    {
        $this->pieceJointe = $pieceJointe;

        return $this;
    }

    public function getStatut(): ?Statut
    {
        return $this->statut;
    }

    public function setStatut(?Statut $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getRecepteur(): ?Users
    {
        return $this->recepteur;
    }

    public function setRecepteur(?Users $recepteur): self
    {
        $this->recepteur = $recepteur;

        return $this;
    }

    public function getEmetteur(): ?Users
    {
        return $this->emetteur;
    }

    public function setEmetteur(?Users $emetteur): self
    {
        $this->emetteur = $emetteur;

        return $this;
    }

    /**
     * @return Collection|Objets[]
     */
    public function getIdobjet(): Collection
    {
        return $this->idobjet;
    }

    public function addIdobjet(Objets $idobjet): self
    {
        if (!$this->idobjet->contains($idobjet)) {
            $this->idobjet[] = $idobjet;
            $idobjet->addIdmessage($this);
        }

        return $this;
    }

    public function removeIdobjet(Objets $idobjet): self
    {
        if ($this->idobjet->removeElement($idobjet)) {
            $idobjet->removeIdmessage($this);
        }

        return $this;
    }
}