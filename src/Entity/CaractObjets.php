<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CaractObjets
 *
 * @ORM\Table(name="caract_objets", indexes={@ORM\Index(name="idobject_idx", columns={"idobject"}), @ORM\Index(name="idkeys_idx", columns={"idkeys"})})
 * @ORM\Entity
 */
class CaractObjets
{
    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value;

    /**
     * @var \Keys
     *
     * @ORM\ManyToOne(targetEntity="Keys")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idkeys", referencedColumnName="idkeys")
     * })
     */
    private $idkeys;

    /**
     * @var \Objets
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Objets")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idobject", referencedColumnName="idobjets")
     * })
     */
    private $idobject;

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getIdkeys(): ?Keys
    {
        return $this->idkeys;
    }

    public function setIdkeys(?Keys $idkeys): self
    {
        $this->idkeys = $idkeys;

        return $this;
    }

    public function getIdobject(): ?Objets
    {
        return $this->idobject;
    }

    public function setIdobject(?Objets $idobject): self
    {
        $this->idobject = $idobject;

        return $this;
    }
}