<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Objets
 *
 * @ORM\Table(name="objets", indexes={@ORM\Index(name="user_id_idx", columns={"user_id"}), @ORM\Index(name="idcategories_idx", columns={"idcategories"})})
 * @ORM\Entity
 */
class Objets
{
    /**
     * @var int
     *
     * @ORM\Column(name="idobjets", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idobjets;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=150, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="illustration", type="string", length=50, nullable=false)
     */
    private $illustration;

    /**
     * @var \Categories
     *
     * @ORM\ManyToOne(targetEntity="Categories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idcategories", referencedColumnName="idcategories")
     * })
     */
    private $idcategories;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * })
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Message", inversedBy="idobjet")
     * @ORM\JoinTable(name="message_objets",
     *   joinColumns={
     *     @ORM\JoinColumn(name="idobjet", referencedColumnName="idobjets")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="idmessage", referencedColumnName="idmessage")
     *   }
     * )
     */
    private $idmessage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idmessage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getIdobjets(): ?int
    {
        return $this->idobjets;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIllustration(): ?string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getIdcategories(): ?Categories
    {
        return $this->idcategories;
    }

    public function setIdcategories(?Categories $idcategories): self
    {
        $this->idcategories = $idcategories;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getIdmessage(): Collection
    {
        return $this->idmessage;
    }

    public function addIdmessage(Message $idmessage): self
    {
        if (!$this->idmessage->contains($idmessage)) {
            $this->idmessage[] = $idmessage;
        }

        return $this;
    }

    public function removeIdmessage(Message $idmessage): self
    {
        $this->idmessage->removeElement($idmessage);

        return $this;
    }
}